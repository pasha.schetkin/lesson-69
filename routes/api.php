<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjo
y building your API!
|
*/

Route::apiResource('articles', App\Http\Controllers\ArticlesController::class);

Route::post('register', [App\Http\Controllers\Api\PassportAuthController::class, 'register'])->name('register');

Route::post('login', [App\Http\Controllers\Api\PassportAuthController::class, 'login'])->name('login');
