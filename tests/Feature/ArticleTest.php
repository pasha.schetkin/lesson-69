<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use WithFaker;

    /**
     * @var Collection|User
     */
    private $user;

    /**
     * @var Collection|Model
     */
    private $articles;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = \App\Models\User::factory()->create();
        $this->articles = \App\Models\Article::factory()->count(10)->for($this->user)->create();
    }

    /**
     * Success get all articles.
     * @group articles1
     * @return void
     */
    public function test_can_get_all_articles()
    {
        $request = $this->getJson(route('articles.index'));
        $request->assertSuccessful();
//        $request->dump();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertCount($this->articles->count(), $payload->data);
    }

    /**
     * Failed get article.
     * @group articles2
     * @return void
     */
    public function test_get_article()
    {
        $article = $this->articles->random();
        $request = $this->getJson(route('articles.show', ['article' => $article]));
        $request->assertUnauthorized();
    }

    /**
     * Success get article.
     * @group articles3
     * @return void
     */
    public function test_success_get_article()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.show', ['article' => $article])]
        );
        $request = $this->getJson(route('articles.show', ['article' => $article]));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertArrayHasKey('id', (array)$payload->data);
        $this->assertEquals($article->id, $payload->data->id);
        $this->assertArrayHasKey('title', (array)$payload->data);
        $this->assertEquals($article->title, $payload->data->title);
    }

    /**
     * Success create article.
     * @group articles
     * @return void
     */
    public function test_can_create_article()
    {
        $data = [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertCreated();
        $this->assertDatabaseHas('articles', $data);
        $this->assertDatabaseHas('articles', ['id' => $payload->data->id]);
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_title_validation_error()
    {
        $data = [
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_validation_error()
    {
        $data = [];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
        $this->assertArrayHasKey('content', (array)$payload->errors);
        $this->assertArrayHasKey('user_id', (array)$payload->errors);
    }
}
