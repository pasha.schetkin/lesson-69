import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { useState, useEffect } from "react";
import { post, postFormData } from '../../request';
import {isAuth} from "../../utils";
import LoadingButton from "@mui/lab/LoadingButton";

const CreateArticle = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [article, setDataArticle] = useState({title: '', content: ''});
    const [file, handleFile] = useState(null);
    const token = isAuth();

    useEffect(() => {
        if (!token) {
            window.location.href = '/';
        }
    }, []);

    const handleArticleForm = (event) => {
        setDataArticle({
            ...article,
            [event.target.name]: event.target.value
        });
    }

    const handleAddFile = (event) => {
        console.log(event.target.files[0]);
        handleFile(event.target.files[0]);
    }

    const handleSubmit = (event) => {
        setIsLoading(true);
        event.preventDefault();
        if (file) {
            const formData = new FormData();
            formData.append('image', file, file.name);
            formData.append('title', article.title);
            formData.append('content', article.content);
            console.log(formData);
            const response = postFormData("/articles", formData, token);
            response.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    window.location.replace("/articles");
                } else {
                    console.log(payload);
                }
                setIsLoading(false);
            });

        } else {
            const response = post("/articles", article, token);
            response.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    window.location.replace("/articles");
                } else {
                    console.log(payload);
                }
                setIsLoading(false);
            });
        }
    };

    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': { m: 1, width: '100%' },
            }}
            autoComplete="off"
            onSubmit={handleSubmit}
        >
            <h1>Create new article</h1>
            <TextField
                label="Title"
                required
                id="outlined-size-small"
                value={article.title}
                size="small"
                fullWidth
                name="title"
                onChange={handleArticleForm}
            />
            <TextField
                id="outlined-multiline-static"
                label="Content"
                multiline
                required
                fullWidth
                rows={4}
                name="content"
                value={article.content}
                onChange={handleArticleForm}
            />
            <input type="file" name="image" onChange={handleAddFile}/>
            <LoadingButton
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                loading={isLoading}
            >
                Create
            </LoadingButton>
        </Box>
    );
}

export default CreateArticle;
