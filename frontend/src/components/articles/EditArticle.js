import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { useState, useEffect } from "react";
import {get, put, putFormData} from '../../request';
import {isAuth} from "../../utils";
import LoadingButton from "@mui/lab/LoadingButton";
import { useParams } from 'react-router-dom';


const EditArticle = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [article, setDataArticle] = useState({title: '', content: ''});
    const [file, handleFile] = useState(null);
    const token = isAuth();
    const { articleId } = useParams();

    useEffect(() => {
        if (!token) {
            window.location.href = '/';
        } else {
            setIsLoading(true);
            const response = get(`/articles/${articleId}`, token);
            response.then(res => {
                if (Object.keys(res).includes('data')) {
                    setDataArticle({...res.data});
                } else {
                    console.log(res);
                }
                setIsLoading(false);
            });
        }
    }, []);

    const handleArticleForm = (event) => {
        setDataArticle({
            ...article,
            [event.target.name]: event.target.value
        });
    }

    const handleAddFile = (event) => {
        handleFile(event.target.files[0]);
    }

    const handleSubmit = (event) => {
        setIsLoading(true);
        event.preventDefault();
        if (file) {
            const formData = new FormData();
            formData.append('image', file, file.name);
            formData.append('title', article.title);
            formData.append('content', article.content);
            formData.append('_method', 'PUT');
            console.log(formData);
            const response = putFormData(`/articles/${articleId}`, formData, token);
            response.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    window.location.replace("/articles");
                } else {
                    console.log(payload);
                }
                setIsLoading(false);
            })
        } else {
            const response = put(`/articles/${articleId}`, article, token);
            response.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    window.location.replace("/articles");
                } else {
                    console.log(payload);
                }
                setIsLoading(false);
            })
        }
    };

    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': { m: 1, width: '100%' },
            }}
            autoComplete="off"
            onSubmit={handleSubmit}
        >
            <h1>Create new article</h1>
            <TextField
                label="Title"
                required
                id="outlined-size-small"
                value={article.title}
                size="small"
                fullWidth
                name="title"
                onChange={handleArticleForm}
            />
            <TextField
                id="outlined-multiline-static"
                label="Content"
                multiline
                required
                fullWidth
                rows={4}
                name="content"
                value={article.content}
                onChange={handleArticleForm}
            />
            <input type="file" name="image" onChange={handleAddFile}/>
            {article.image_url &&
                <p>
                    <img src={article.image_url} alt={article.title} width="150px" height="150px" />
                </p>
            }
            <LoadingButton
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                loading={isLoading}
            >
                Update
            </LoadingButton>
        </Box>
    );
}

export default EditArticle;
