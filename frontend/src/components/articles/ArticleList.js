import React, {useState, useEffect} from 'react';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import {styled} from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, {tableCellClasses} from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import {Link as RouterLink} from "react-router-dom";
import {get, remove} from '../../request';
import TablePagination from '@mui/material/TablePagination';
import {isAuth} from "../../utils";

const StyledTableCell = styled(TableCell)(({theme}) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({theme}) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const Articles = () => {
        const [isLoading, setIsLoading] = useState(false);
        const [articles, setArticles] = useState(null);
        const [countArticles, setCountArticles] = useState(0);

        const [page, setPage] = useState(0);
        const [rowsPerPage, setRowsPerPage] = useState(10);
        const token = isAuth();

        const handleChangePage = (event, newPage) => {
            setPage(newPage);
        };

        const handleDeleteArticle = (articleForDelete) => {
            setIsLoading(true);
            const response = remove(`/articles/${articleForDelete.id}`, token);
            response.then(() => {
                const newA = articles.filter(article => articleForDelete.id !== article.id);
                setArticles(newA);
                setCountArticles(countArticles - 1);
                setIsLoading(false);
            })
        }

        const handleChangeRowsPerPage = (event) => {
            setRowsPerPage(+event.target.value);
            setPage(0);
        };

        useEffect(() => {
            getArticles();
        }, []);

        useEffect(() => { getArticles(); }, [rowsPerPage, page])

        const getArticles = () => {
            setIsLoading(true);
            let query = '';
            if (page >= 1) {
                query = `?page=${page+1}&per_page=${rowsPerPage}`;
            } else {
                query = `?per_page=${rowsPerPage}`;
            }
            const response = get(`/articles${query}`, token);
            response.then(data => {
                setArticles(data.data);
                setCountArticles(data?.meta?.total);
                setIsLoading(false);
            });
        }
        return (
            <div style={{paddingBottom: '50px'}}>
                <div>
                    <h1>
                        Articles
                        <Button component={RouterLink} to={'/articles/create'} variant="outlined">
                            Create new article
                        </Button>
                    </h1>
                </div>
                {isLoading &&
                <Box sx={{width: '100%'}}>
                    <LinearProgress/>
                </Box>
                }
                {articles &&
                <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                    <TableContainer component={Paper}>
                        <Table sx={{minWidth: 700}} aria-label="customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell align="center">Id</StyledTableCell>
                                    <StyledTableCell align="center">Title</StyledTableCell>
                                    <StyledTableCell align="center">Body</StyledTableCell>
                                    <StyledTableCell align="center">Author</StyledTableCell>
                                    <StyledTableCell align="center">Image</StyledTableCell>
                                    <StyledTableCell align="center">Actions</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {articles.map((article, key) => (
                                    <StyledTableRow key={key}>
                                        <StyledTableCell align="center">{article.id}</StyledTableCell>
                                        <StyledTableCell align="center">{article.title}</StyledTableCell>
                                        <StyledTableCell align="center">{article.content}</StyledTableCell>
                                        <StyledTableCell align="center">{article.user.name}</StyledTableCell>
                                        <StyledTableCell align="center">
                                            {article.image_url &&
                                                <img src={article.image_url} alt={article.title} width="50px" height="50px" />
                                            }
                                        </StyledTableCell>
                                        <StyledTableCell align="center">
                                            <ButtonGroup variant="text" aria-label="text button group">
                                                <Button component={RouterLink} to={`/articles/${article.id}`}>Show</Button>
                                                <Button component={RouterLink} to={`/articles/edit/${article.id}`}>Edit</Button>
                                                <Button onClick={() => handleDeleteArticle(article)}>Destroy</Button>
                                            </ButtonGroup>
                                        </StyledTableCell>
                                    </StyledTableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={countArticles}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                    />
                </Paper>
                }
            </div>
        );
    }
;

export default Articles;
