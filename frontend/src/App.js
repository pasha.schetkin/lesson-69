import React, {Suspense} from 'react';
import Navbar from "./components/common/Navbar";
import Container from '@mui/material/Container';
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import routes from "./routes";
import Home from "./components/common/Home";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";

function App() {
    const getRoutes = (routes) => {
        return routes.map((prop, key) => {
            return (
                <Route
                    path={prop.path}
                    key={key}
                    element={prop.element}
                />
            );
        });
    };

    return (
        <div>
            <Suspense fallback={<div>Загрузка...</div>}>
                <Navbar routes={routes}/>
                <Container style={{paddingTop: '20px'}}>
                    <Routes>
                        {getRoutes(routes)}
                        <Route path="*" element={<Home/>}/>
                        <Route path="/auth/login" element={<Login/>}/>
                        <Route path="/auth/register" element={<Register/>}/>
                    </Routes>
                </Container>
            </Suspense>
        </div>
    );
}

export default App;
